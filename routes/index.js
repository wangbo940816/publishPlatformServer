/**
 * 主路由文件
 * index.js
 * @author wangbo
 * @since 2021/3/11
 */
const router = require('koa-router')();
const testModel = require('../models/test');
router.prefix('/api');

router.get('/', async (ctx, next) => {
	ctx.body = {
		title: '默认路由'
	}
})

router.get('/string', async (ctx, next) => {
	ctx.body = 'koa2 string'
})

router.get('/json', async (ctx, next) => {
	ctx.body = {
		title: 'koa2 json'
	}
})


router.get('/query', async function (ctx, next) {
	let data = await testModel.query();
	console.log(data);
	ctx.body = data;
})

router.get('/save', async function (ctx, next) {
	const saveData = {
		title: '1111',
		body: 2222,
		date: new Date()
	};
	let data = await testModel.save(saveData);
	ctx.body = '保存成功';
})

module.exports = router;
