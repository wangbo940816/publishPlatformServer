/**
 * 数据库配置文件
 * db.js
 * @author wangbo
 * @since 2021/3/11
 */
const mongoose = require('mongoose');
const DBConfig = require('../constants');
const {host, dbName, pass, port, user} = DBConfig;
const DB_URL = `mongodb://${host}:${port}/${dbName}`;

mongoose.connect(DB_URL, {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useFindAndModify: false,
	useCreateIndex: true,
	user,
	pass
});

mongoose.connection.on('connected', function () {
	console.log('Mongoose connection open to ' + DB_URL);
});
/**
 * 连接异常 error 数据库连接错误
 */
mongoose.connection.on('error', function (err) {
	console.log('Mongoose connection error: ' + err);
});
/**
 * 连接断开 disconnected 连接异常断开
 */
mongoose.connection.on('disconnected', function () {
	console.log('Mongoose connection disconnected');
});

module.exports = mongoose;
