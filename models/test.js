/**
 * test.js
 * @author wangbo
 * @since 2021/3/11
 */

const mongoose = require('../config/db');
const Schema = mongoose.Schema;

const testSchema = new Schema({
	title: String,
	body: String,
	date: Date
});

// name进入数据库后会名称自动变为复数
const MyModel = mongoose.model('users', testSchema);

class Mongodb {
	// 查询
	query() {
		return new Promise((resolve, reject) => {
			MyModel.find({}, (err, res) => {
				if (err) {
					reject(err)
				}
				resolve(res)
			})
		})
	};

	// 保存
	save(obj) {
		const m = new MyModel(obj);
		return new Promise((resolve, reject) => {
			m.save((err, res) => {
				if (err) {
					reject(err)
				}
				resolve(res)
				console.log(res)
			})
		})
	};
}

module.exports = new Mongodb();
