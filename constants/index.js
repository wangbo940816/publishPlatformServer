/**
 * 常量
 * index.js
 * @author wangbo
 * @since 2021/3/11
 */
module.exports = DBConfig = {
	host: 'localhost',
	port: 27017,
	dbName: 'publishData',
	user: '',
	pass: ''
};
